<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property string $urgency
 * @property string $created_at
 * @property string $updated_at
 * @property int $crated_by
 * @property int $updated_by
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['crated_by', 'updated_by'], 'integer'],
            [['name', 'urgency'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'urgency' => 'Urgency',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'crated_by' => 'Crated By',
            'updated_by' => 'Updated By',
        ];
    }
       public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'updated_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
    
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'crated_by',
                'updatedByAttribute' => 'updated_by',
            ],
    
            // for different configurations, please see the code
            // we have created tables and relationship in order to
            // use defaults settings
        
        ];
    }
     public function getUser()
    {
      return $this->hasOne(User::className(),['id'=>'crated_by']); 
    }
        public function getUrgency()
    {
      return $this->hasOne(Urgency::className(),['id'=>'urgency']); 
    }
}

