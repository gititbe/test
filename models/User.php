<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 * @property int $crated_by
 * @property int $updated_by
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['crated_by', 'updated_by'], 'integer'],
            [['name', 'username', 'password', 'auth_key'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

public static function findIdentity($id)
{
return static::findOne($id);
}

public static function findIdentityByAccessToken($token, $type = null)
{
// return static::findOne(['access_token' => $token]);
}

public function getId()
{
return $this->id;
}

public function getAuthKey()
{
return $this->auth_key;
}

public function validateAuthKey($authKey)
{
return $this->auth_key === $authKey;

}
public static function findByUsername($username)
{
return self::findOne(['username'=>$username]);
}

public function validatePassword($password){
return \Yii::$app->security->validatePassword($password,$this->password);
}

public function beforeSave($insert){
if(parent::beforeSave($insert)){
if($this->isNewRecord){
$this->auth_key = \Yii::$app->security->generateRandomString(); //נשמר

}
if($this->isAttributeChanged('password')) //מונע האש על האש.זה תהליך ההצפנה
{
$this->password = \Yii::$app->security->generatePasswordHash($this->password);
}
return true;
}
return false;
}
/**
* @inheritdoc
*/

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'crated_by' => 'Crated By',
            'updated_by' => 'Updated By',
        ];
    }
       public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'updated_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
    
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'crated_by',
                'updatedByAttribute' => 'updated_by',
            ],
    
            // for different configurations, please see the code
            // we have created tables and relationship in order to
            // use defaults settings
        
        ];
    }
}
