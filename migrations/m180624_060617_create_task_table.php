<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m180624_060617_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
           'urgency' => $this->string(),
           'created_at' => $this->timestamp(),
           'updated_at' => $this->timestamp(),
           'crated_by' => $this->integer(),
           'updated_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}
