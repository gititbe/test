<?php

use yii\db\Migration;

/**
 * Class m180624_093236_init_rbac
 */
class m180624_093236_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
 {
             
        $auth = Yii::$app->authManager;
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        
        $employee = $auth->createRole('employee');
        $auth->add($employee);
  
         
        $auth->addChild($admin, $employee);
        
        $editMyUser = $auth->createPermission('editMyUser'); 
        
        $editUser = $auth->createPermission('editUser'); 
        $auth->add($editUser);
        
        $veiwUser = $auth->createPermission('veiwUser'); 
        $auth->add($veiwUser);
        
        $veiwUsers = $auth->createPermission('veiwUsers'); 
        $auth->add($veiwUsers);


        $createTask = $auth->createPermission('createTask'); 
        $auth->add($createTask);

        $editTask = $auth->createPermission('editTask'); 
        $auth->add($editTask);

        $deletTask = $auth->createPermission('deletTask'); 
        $auth->add($deletTask);

    
        $rule = new \app\rbac\AuthorRule;
        $auth->add($rule);
    
        $editMyUser->ruleName = $rule->name;                
        $auth->add($editMyUser);                 
                                  
     
        $auth->addChild($employee, $editMyUser); 
        $auth->addChild($employee, $veiwUsers);
        $auth->addChild($employee, $veiwUser);
        $auth->addChild($employee, $createTask); 
        $auth->addChild($employee, $editTask);


        $auth->addChild($admin, $deletTask);
        $auth->addChild($admin, $employee);
                
        
        $auth->addChild($editMyUser, $editUser); 
    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_093236_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_093236_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
