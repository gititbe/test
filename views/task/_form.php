<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use yii\helpers\ArrayHelper;
use app\models\Urgency;

/* @var $this yii\web\View */
/* @var $model app\models\task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
</div>
  <?= $form->field($model, 'urgency')->dropDownList(
    ArrayHelper::map(Urgency::find()->asArray()->all(), 'id', 'name')//מציג את כל הרשומות מטבלת category ורק את השדות איי די וניימ
) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

